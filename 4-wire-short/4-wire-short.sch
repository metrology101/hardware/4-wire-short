EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "4 wire DMM short "
Date "2020-09-27"
Rev "0.1"
Comp "Konopnickiej.Com"
Comment1 "Author: Paweł 'felixd' Wojciechowski"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 4-wire-short-rescue:MountingHole_Pad-Mechanical-4-wire-short-rescue H4
U 1 1 5F707336
P 3300 1950
F 0 "H4" H 3050 2000 50  0000 L CNN
F 1 "LO" H 3050 1900 50  0000 L CNN
F 2 "MountingHole:MountingHole_4.3mm_M4_DIN965_Pad" H 3300 1950 50  0001 C CNN
F 3 "~" H 3300 1950 50  0001 C CNN
	1    3300 1950
	-1   0    0    1   
$EndComp
$Comp
L 4-wire-short-rescue:MountingHole_Pad-Mechanical-4-wire-short-rescue H3
U 1 1 5F706AD1
P 3300 1750
F 0 "H3" H 3450 1800 50  0000 L CNN
F 1 "HI" H 3450 1700 50  0000 L CNN
F 2 "MountingHole:MountingHole_4.3mm_M4_DIN965_Pad" H 3300 1750 50  0001 C CNN
F 3 "~" H 3300 1750 50  0001 C CNN
	1    3300 1750
	1    0    0    -1  
$EndComp
$Comp
L 4-wire-short-rescue:MountingHole_Pad-Mechanical-4-wire-short-rescue H2
U 1 1 5F7076B2
P 3000 1950
F 0 "H2" H 3100 1999 50  0000 L CNN
F 1 "LO Sense" H 3100 1908 50  0000 L CNN
F 2 "MountingHole:MountingHole_4.3mm_M4_DIN965_Pad" H 3000 1950 50  0001 C CNN
F 3 "~" H 3000 1950 50  0001 C CNN
	1    3000 1950
	-1   0    0    1   
$EndComp
$Comp
L 4-wire-short-rescue:MountingHole_Pad-Mechanical-4-wire-short-rescue H1
U 1 1 5F706674
P 3000 1750
F 0 "H1" H 2800 1800 50  0000 L CNN
F 1 "HI Sense" H 2550 1700 50  0000 L CNN
F 2 "MountingHole:MountingHole_4.3mm_M4_DIN965_Pad" H 3000 1750 50  0001 C CNN
F 3 "~" H 3000 1750 50  0001 C CNN
	1    3000 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 1850 3300 1850
Connection ~ 3000 1850
Connection ~ 3300 1850
$EndSCHEMATC
