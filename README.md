# 4-wire short 2020 - Konopnickiej.Com

* gold plated PCB - ENIG (RoHS)
* 18 mm long, 4 mm gold plated brass banana plugs (springs are made from rigid hard-drawn copper alloy sheet)
* M4 screws / holes in PCB have 4.3 mm diameter
* materials are selected to have minimal thermal EMF
* holes separation: 19,05 mm x 19,05 to 34,50 mm (should fit almost any bench DMM)

## 4-Wire Short 2020 replaces
* Keithley General Purpose 4-Wire DMM Shorting Plug Model 8620 and 8610
* Fluke 884X Short 4-Wire Short
* HP, Keysight 34172B Calibration Short for Digital Multimeters

## Board

* PCB size: 30,05 x 44,50 mm
* Holes size: 4,3 mm (M4)
* Holes separation:
    * Vertically: 19,05 mm
    * Horizontally: 19,05 mm - 34,50 mm (centers of holes)

![4-wire short with banana plugs](images/4-wire-short-with-banana-plugs.png)

![4-wire short with banana plugs](images/4-wire-short-v1-front.png)

![4-wire short with banana plugs](images/4-wire-short-v1-back.png)

### Prototype tests

Thanks to **Jay_Diddy_B** from EEVBlog Forum
https://www.eevblog.com/forum/profile/?u=31556

![4-wire short - HP 3457A](images/3457A-22mm.jpg)

![4-wire short - HP,Agilent 34401A](images/34401A-19_05mm-rear.jpg)

![4-wire short - Datron 1281](images/Datron-1281-26mm.jpg)

## List of tested DMMs

* HP 34401A (Agilent 34401A)
* HP 3457A
* Datron 1281
* Keithley 2001 (Keithley 2000, Keithley 2002)

## Plugs

For this project I am using gold plated banana plugs from MultiContact (Stäubli group): 22.1050 - SA400 (with M4 threads and twisted spring for one of the lowest contact resistance possible - 0.2 mOhm)

* https://ec.staubli.com/AcroFiles/Catalogues/IS_PL-Main-MLPlugs-11013983_(en)_hi.pdf

![Plug render](docs/plug-render.jpg)
![Plug Technical Data](docs/plug-technical-data.png)
![Plug Drawing](docs/plug-technical-drawing.png)
![Plug Twisted vs Straight](docs/plug-twisted-vs-straight.png)

### Other plugs that had been checked

### Alpha 3 | Detron 

A contact to company has been made. They do not have gold plated version by default + nickel plated version has around 10 mOhms contact resistance.

* 559-0010 4mm Stud Mounting Bunch Pin Plug
* https://www.dem-uk.com/deltron-components/products/4mm_single_pole_connectors/other_electrical_connectors/559-0010.asp
* https://www.farnell.com/cad/1760899.pdf

## Discussions
* https://www.circuitsonline.net/forum/view/147363
* https://www.circuitsonline.net/forum/view/142444
* https://www.eevblog.com/forum/metrology/binding-postsbanana-sockets-cleaning-on-dmmsmetrology-lab-gear/
* https://www.eevblog.com/forum/oshw/4-wire-short/

## Other products
* GLK Insturments - https://www.glkinst.com/test-equipment/
    * $37.50 - P/N 29034401-SHORT
    * $37.50 - P/N 2903458-SHORT 
* https://www.eevblog.com/forum/oshw/4-wire-short/
* FLUKE
    * $50.00 - Fluke 884X Short 4-Wire Short https://www.fluke.com/en-us/product/accessories/other/884x-short-4-wire-short
* Keithley
    * $75 - $100 - General Purpose 4-Wire DMM Shorting Plug Model 8620
    * $80 - $100 - General Purpose 4-Wire DMM Shorting Plug Model 8610
* Keysight, HP
    * $217 - 34172B - Calibration short

## METROLOGY 101 - Worth reading

Watch Out for Those Thermoelectric Voltages! by Martin L. Kidd Fluke Corporation
* http://download.flukecal.com/pub/literature/p18-21.pdf

How cables and connectors impact measurement uncertainty - Fluke application note
* http://support.fluke.com/find-sales/download/asset/2548277_6001_eng_a_w.pdf

Switching Low-Level Signals to a DMM - National Instruments
* https://www.ni.com/pl-pl/innovations/white-papers/06/switching-low-level-signals-to-a-dmm.html

### 3458A Calibration manual

![4-temrinal short from copper wire](images/3458a-copper-wire.png)

Just use bare, clean (no oxidation layer) 12 - 14 gauge copper wire. No need to use special PCB :)

#### NOTE

`Take precautions to prevent thermal changes near the 4-wire short. You should not touch the short after it is installed. If drafts exist, you should cover the input terminals/short to minimize the thermal changes.`

**Front Terminal Offset Adjustment**

This adjustment uses an external 4-terminal short. The multimeter makes offset measurements and stores constants for the DCV, DCI, OHM, and OHMF functions. These constants compensate for internal offset errors for front terminal measurements.

Equipment required: A low-thermal short made of 12 or 14 gauge solid copper wire as shown in Figure 3-1 on page 39.

1. Make sure you have performed the steps described previously under “Preliminary Adjustment Procedures”.
2. Connect a 4-terminal short across the front panel HI and LO Input terminals and the HI and LO W Sense terminals as shown in Figure 3-1 on page 39.
3. After connecting the 4-terminal short, allow 5 minutes for thermal equilibrium.
4. Execute the CAL 0 command. The multimeter automatically performs the front terminal offset adjustment and the display shows each of the various steps being performed. This adjustment takes about 5 minutes. When the adjustment is complete, the multimeter returns to displaying DC voltage measurements.

## Paired Materials

```
Paired Materials             μV/°C (Q ab )
Copper-Copper                <0.2
Copper-Cadmium/Tin Solder    0.2
Copper-Gold                  0.3
Copper-Silver                0.3
Copper-Brass                 3
Copper-Lead-Tin Solder       5
Copper-Aluminum              5
Copper-Nickel                10
Copper-Kovar                 40
Copper-Copper Oxide          >1000
```

## Copper thickness

```
1 oz Copper Thickness Conversion
1 oz = 1.37 mils (thousandths of an inch)
0.00137 inch = 0.0347 mm (34.79 µm (micron/micro meter))
```


## Author
* Paweł 'felixd' Wojciechowski (Konopnickiej.Com)

